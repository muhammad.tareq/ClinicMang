function deleteObject(url, id) {
    var url = url + "/" + id;
    var token = $('meta[name="csrf-token"]').attr('content');
    swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "yes, Delete!",
            cancelButtonText: "No",
            closeOnConfirm: true
        },
        function () {
            $.ajax({
                url: url,
                type: 'delete',
                dataType: "JSON",
                data: {"_token": token},
                success: function (data) {
                    if (data == 'success') {
                        $('#ddata-' + id).remove();
                    }

                }

            });
        });
}