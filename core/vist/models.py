from django.db import models
from client.models import Client
# Create your models here.

class Visit(models.Model):
    note = models.TextField(max_length=500)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    cost = models.FloatField(default=0)
    paid = models.FloatField(default=0)
    attachment = models.FileField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.client.name
