from django.shortcuts import render
from uuid import getnode
from django.http import HttpResponse
from django.views.generic import ListView, DeleteView, CreateView, UpdateView
from django.urls import reverse_lazy
from .models import Visit
from client.models import Client


# Create your views here.


class VisitListView(ListView):
    model = Visit
    template_name = "partials/visits/list_view.html"


class AddVisiToClientView(CreateView):
    model = Visit
    template_name = "partials/visits/add_view.html"
    fields = ['note', 'attachment']

    def form_valid(self, form):
        client = Client.objects.get(pk=self.kwargs['pk'])
        form.instance.client = client
        form.save()
        return super(AddVisiToClientView, self).form_valid(form)
        # return super(AddVisiToClientView, self).form_invalid(form)

    def get_success_url(self):
        return reverse_lazy('client:show', args=(self.object.client.id,))
