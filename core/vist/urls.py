
from django.conf.urls import url
from .views import VisitListView,AddVisiToClientView
from django.views.generic.base import RedirectView
urlpatterns = [
    url(r'^$', VisitListView.as_view(), name="index"),
    url(r'^(?P<pk>\d+)/add/$', AddVisiToClientView.as_view(), name="add"),
    # url(r'^(?P<pk>\d+)/delete/$', ClientDeleteView.as_view(), name="delete"),
    # url(r'^(?P<pk>\d+)/update/$', UpdateClientView.as_view(), name="update"),
    # url(r'^search/$', TweetListView.as_view(), name="list"),


]
