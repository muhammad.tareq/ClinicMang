from django import forms
from .models import Client
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
)


class AddClientForm(forms.Form):
    name = forms.TextInput(attrs={'class': 'form-control'})
    mobile = forms.TextInput(attrs={'class': 'form-control'})


class UserLoginForm(forms.Form):
    username = forms.CharField(
        required=True,
        label="اسم المستخدم",
        widget=forms.TextInput(attrs={'class': 'form-control'})
    )
    password = forms.CharField(
        required=True,
        label="كلمة المرور",
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))

    # def clean(self, *args, **kwargs):
    #     username = self.cleaned_data.get("username")
    #     password = self.cleaned_data.get("password")
    #
    #     user = authenticate(username=username, password=password)
    #     print(user)
    #
    #     if user is None:
    #
    #         raise forms.ValidationError("This user does not exist")
    #
    #     return super(UserLoginForm, self).clean(*args, **kwargs)
