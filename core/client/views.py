from uuid import getnode
from .models import Client
from .forms import AddClientForm, UserLoginForm
from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpRequest
from django.contrib import messages

from django.http import HttpResponseRedirect

from django.urls import reverse_lazy, reverse
from django.views.generic import ListView, DeleteView, CreateView, UpdateView, DetailView, View
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
)


class login_view(View):
    form = UserLoginForm
    template_name = "auth/login.html"
    context = {'form': form}

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('client:index'))

        context = self.context
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        context = self.context
        next = request.GET.get('next')
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            user = authenticate(username=username, password=password)
            if user is None:
                messages.error(
                    request, 'عفوا، هذا المستخدم غير موجود.')
                return render(request, self.template_name, context)

            else:
                login(request, user)
        if next:
            return redirect(next)
        return redirect('/appointment/list')


class logout_view(View):
    def get(self, request, *args, **kwargs):
        return redirect('/')

    def post(self, request, *args, **kwargs):
        logout(request)
        return HttpResponseRedirect(reverse('login'))


# Create your views here.

class ClientListView(ListView):
    model = Client
    template_name = "partials/clients/list_view.html"


class ClientDeleteView(DeleteView):
    model = Client
    template_name = "partials/clients/delete_confirm.html"
    success_url = reverse_lazy("client:index")


class AddClientView(CreateView):
    model = Client
    # form_class = AddClientForm
    template_name = "partials/clients/add_view.html"
    fields = ['name', 'mobile', 'diagnose']

    def form_valid(self, form):
        form.save()
        return super(AddClientView, self).form_valid(form)

    success_url = reverse_lazy("client:index")


class UpdateClientView(UpdateView):
    model = Client
    template_name = "partials/clients/add_view.html"
    fields = ['name', 'mobile', 'diagnose']
    success_url = reverse_lazy("client:index")


class DetailClientView(DetailView):
    model = Client
    template_name = "partials/clients/detail_view.html"
