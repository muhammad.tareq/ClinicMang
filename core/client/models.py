from django.db import models


# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=60)
    mobile = models.CharField(max_length=20, unique=True)
    diagnose = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    # def get_due(self):
    #     total_paid = 0
    #     total_cost = 0
    #
    #     for amount in self.visit_set.all():
    #         total_paid += amount.paid
    #         total_cost += amount.cost
    #     return {"total_paid":total_paid,"total_cost":total_cost,"residual_debt":total_cost -total_paid}
