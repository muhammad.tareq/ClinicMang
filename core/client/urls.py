
from django.conf.urls import url
from .views import ClientListView, ClientDeleteView, AddClientView, UpdateClientView,DetailClientView
from django.views.generic.base import RedirectView
urlpatterns = [
    url(r'^$', ClientListView.as_view(), name="index"),
    url(r'^add/$', AddClientView.as_view(), name="add"),
    url(r'^(?P<pk>\d+)/delete/$', ClientDeleteView.as_view(), name="delete"),
    url(r'^(?P<pk>\d+)/update/$', UpdateClientView.as_view(), name="update"),
    url(r'^(?P<pk>\d+)/show/$', DetailClientView.as_view(), name="show"),
    # url(r'^search/$', TweetListView.as_view(), name="list"),


]
