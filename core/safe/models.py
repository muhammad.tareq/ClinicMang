from django.db import models
from client.models import Client


# Create your models here.

class Safe(models.Model):
    client = models.ForeignKey(Client)
    amount = models.FloatField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.client.name + " " + self.client.mobile

    def get_paid(self):
        total_paid = 0
        for payment in self.payments_set.all():
            total_paid += payment.amount
        return total_paid

class Payments(models.Model):
    safe = models.ForeignKey(Safe)
    amount = models.FloatField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.created_at

    def get_user(self):
        return self.safe.client.name