from django.conf.urls import url
from .views import AddBillToClient, GetClientBills,UpdateSafeView,SafeDetailView,AddPaymentToClient,GetPaymentsByDay
from django.views.generic.base import RedirectView

urlpatterns = [
    # url(r'^$', VisitListView.as_view(), name="index"),
    url(r'^(?P<pk>\d+)/add/$', AddBillToClient.as_view(), name="add"),
    url(r'^(?P<pk>\d+)/list/$', GetClientBills.as_view(), name="list"),
    url(r'^(?P<pk>\d+)/detail/$', SafeDetailView.as_view(), name="detail"),
    url(r'^(?P<pk>\d+)/add/payment/$', AddPaymentToClient.as_view(), name="add_payment"),
    url(r'^(?P<pk>\d+)/update/$', UpdateSafeView.as_view(), name="update"),
    url(r'^payments/$', GetPaymentsByDay.as_view(), name="filter"),
    # url(r'^search/$', TweetListView.as_view(), name="list"),

]
