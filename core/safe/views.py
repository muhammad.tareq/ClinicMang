from django.shortcuts import render
from django.contrib import messages
from django.views.generic import View, UpdateView, DetailView
from client.models import Client
from .models import Safe, Payments
from .forms import AddPaymentForm, AddBillForm
from django.contrib.messages.views import SuccessMessageMixin

from django.urls import reverse_lazy, reverse
from django.shortcuts import get_object_or_404, render
from django.http import JsonResponse, HttpResponseRedirect


# Create your views here.



class AddBillToClient(View):
    template_name = "partials/safe/safe_add_view.html"

    the_form = AddBillForm

    def get(self, request, *args, **kwargs):
        client = Client.objects.get(pk=kwargs['pk'])
        # safe = client.safe_set.first()
        context = {'client': client, 'form': self.the_form}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.the_form(request.POST)
        client = Client.objects.get(pk=kwargs['pk'])
        if form.is_valid():
            safe = form.save(commit=False)
            safe.client = client
            # <process form cleaned data>
            safe.save()
            payment = Payments()
            payment.safe = safe
            payment.amount = request.POST.get('to_pay')
            payment.save()
            messages.success(
                request, 'Client %s has.' % client.name)

        return HttpResponseRedirect(reverse('safe:detail', kwargs={'pk': safe.id}))


class GetClientBills(View):
    template_name = "partials/safe/safe_client_list_view.html"

    # the_form = AddBillForm
    def get(self, request, *args, **kwargs):
        client = Client.objects.filter(pk=kwargs['pk']).first()
        safe = client.safe_set.all()
        context = {'safe': safe, 'client': client.name + " " + client.mobile}
        return render(request, self.template_name, context)


class UpdateSafeView(SuccessMessageMixin, UpdateView):
    model = Safe
    template_name = "partials/safe/safe_add_view.html"
    fields = ['amount']
    success_message = 'Amount has been Saved successfully.'

    def get_object(self, queryset=None):
        object = Safe.objects.get(id=self.kwargs['pk'])
        return object

    def get_success_url(self):
        return reverse('safe:list', kwargs={'pk': self.kwargs['pk']})


class SafeDetailView(View):
    template_name = "partials/safe/list_view.html"

    def get(self, request, *args, **kwargs):
        object = Safe.objects.get(id=kwargs['pk'])
        object_list = object.payments_set.all()
        overall_amount = object.amount
        context = {'object_list': object_list, 'overall': overall_amount,
                   'paid': object.get_paid(),
                   'client': object.client.name + " " + object.client.mobile}
        return render(request, self.template_name, context)


class AddPaymentToClient(View):
    template_name = "partials/safe/add_view.html"
    form = AddPaymentForm

    def get(self, request, *args, **kwargs):
        safe = Safe.objects.get(pk=kwargs['pk'])
        client = safe.client
        # client = Client.objects.get(pk=kwargs['pk'])
        context = {'form': self.form, 'client': client.name + " " + client.mobile}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        safe = Safe.objects.get(pk=kwargs['pk'])
        context = {'form': self.form, 'client': safe.client.name + " " + safe.client.mobile}
        total_amount = safe.amount
        if (total_amount - safe.get_paid()) >= float(request.POST.get('amount')):
            if form.is_valid():
                # <process form cleaned data>
                payment = form.save(commit=False)
                payment.safe = safe
                payment.save()
                messages.success(
                    request, 'Payment has been added successfully.')
                # return render(request, self.template_name, context)
            return HttpResponseRedirect(reverse('safe:detail', kwargs={'pk': safe.id}))
        else:
            messages.error(request, 'The payment is greater than the due.')
            return render(request, self.template_name, context)


class GetPaymentsByDay(View):
    template_name = "partials/safe/list_view.html"

    def get(self, request, *args, **kwargs):
        payments = Payments.objects.filter(created_at__icontains=request.GET.get('date'))
        overall = 0
        for payment in payments:
            overall += payment.amount
        context = {'object_list':payments,'overall':overall}
        return render(request, self.template_name, context)
