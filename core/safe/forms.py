from django import forms
from .models import Safe, Payments


class AddBillForm(forms.ModelForm):
    class Meta:
        model = Safe
        fields = ['amount']
        labels = {
            "amount": "القيمة"
        }
        widgets = {
            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
        }

#
# class AddBillForm(forms.ModelForm):
#     class Meta:
#         model = Safe
#         fields = ['client', 'total', 'paid']
#         widgets = {
#             'client': forms.Select(attrs={'class': 'form-control'}),
#             'total': forms.NumberInput(attrs={'class': 'form-control'}),
#             'paid': forms.NumberInput(attrs={'class': 'form-control'}),
#         }

class AddPaymentForm(forms.ModelForm):
    class Meta:
        model = Payments
        labels={
            "amount":"القيمة"
        }
        fields = ['amount']
        widgets = {
            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
        }
