import re

from django.conf import settings
from django.shortcuts import redirect
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.utils.http import is_safe_url
EXEMPT_URLS = [re.compile(settings.LOGIN_URL.lstrip('/'))]

if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [re.compile(url) for url in settings.LOGIN_EXEMPT_URLS]


class LoginRequiredMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        assert hasattr(request, 'user')
        path = request.path_info.lstrip('/')

        url_is_exempt = any(url.match(path) for url in EXEMPT_URLS)

        # if request.user.is_authenticated() and url_is_exempt:
        #     return redirect(settings.LOGIN_REDIRECT_URL)
        #
        # elif request.user.is_authenticated() or url_is_exempt:
        #     return None
        #
        # else:
        #     return redirect(settings.LOGIN_URL)

        if not request.user.is_authenticated():
            if not url_is_exempt:
                redirect_to = settings.LOGIN_URL
                # Add 'next' GET variable to support redirection after login
                if len(path) > 0 and is_safe_url(url=request.path_info, host=request.get_host()):
                    redirect_to = "%s?next=%s" %(settings.LOGIN_URL, request.path_info)
                return HttpResponseRedirect(redirect_to)
