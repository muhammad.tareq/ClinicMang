"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from client.views import login_view, logout_view

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('client.urls')),
    url(r'^clients/', include('client.urls', namespace='client')),
    url(r'^visit/', include('vist.urls', namespace='visit')),
    url(r'^safe/', include('safe.urls', namespace='safe')),
    url(r'^appointment/', include('appointments.urls', namespace='appointment')),
    url(r'^lab/', include('labs.urls', namespace='labs')),
    url(r'^login/$', login_view.as_view(), name='login'),
    url(r'^logout/$', logout_view.as_view(), name='logout'),

]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
