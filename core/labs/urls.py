from django.conf.urls import url
from .views import (
    ListLabs, AddLab, DeleteLab, AddItemType, ListItemType,
    AddItemToLab, LabDetailView, DeleteItemFromLab, LabsDue,
    DeleteItemType
)
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^$', ListLabs.as_view(), name="index"),
    url(r'^itemstype$', ListItemType.as_view(), name="item-type-index"),
    url(r'^add/$', AddLab.as_view(), name="add"),
    url(r'^add/item/type$', AddItemType.as_view(), name="add-item-type"),
    url(r'^due/$', LabsDue.as_view(), name="due"),
    url(r'^add/item/$', AddItemToLab.as_view(), name="add-item"),
    url(r'^(?P<pk>\d+)/delete/item/$', DeleteItemFromLab.as_view(), name="delete-item"),
    url(r'^(?P<pk>\d+)/delete/item/type$', DeleteItemType.as_view(), name="delete-item-type"),
    url(r'^(?P<pk>\d+)/delete/$', DeleteLab.as_view(), name="delete"),
    # url(r'^(?P<pk>\d+)/update/$', UpdateClientView.as_view(), name="update"),
    url(r'^(?P<pk>\d+)/show/$', LabDetailView.as_view(), name="show"),
    # url(r'^search/$', TweetListView.as_view(), name="list"),

]
