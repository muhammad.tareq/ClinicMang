from django.shortcuts import render
from .models import Lab, Item, ItemType
from django.urls import reverse_lazy, reverse
from .forms import AddLabForm, AddItemToLabForm, AddItemTypeForm
from django.views.generic import View, CreateView, ListView, DeleteView, DetailView


# Create your views here.


class ListLabs(ListView):
    model = Lab
    template_name = "partials/labs/list_view.html"


class AddLab(CreateView):
    model = Lab
    form_class = AddLabForm
    template_name = "partials/labs/add_view.html"
    success_url = reverse_lazy("labs:index")


class DeleteLab(DeleteView):
    model = Lab
    template_name = "partials/labs/delete_view.html"
    success_url = reverse_lazy("labs:index")


class AddItemToLab(CreateView):
    model = Item
    form_class = AddItemToLabForm
    template_name = "partials/labs/item/add_view.html"
    success_url = reverse_lazy("labs:index")


class LabDetailView(DetailView):
    model = Lab
    template_name = "partials/labs/detail_view.html"


class DeleteItemFromLab(DeleteView):
    model = Item
    template_name = "partials/labs/delete_view.html"
    success_url = reverse_lazy("labs:index")


class LabsDue(View):
    template_name = "partials/labs/due_list_view.html"

    def get(self, request, *args, **kwargs):
        object_list = Item.objects.filter(created_at__range=(request.GET.get('from'), request.GET.get('to')))
        overall = 0
        for item in object_list:
            overall += item.amount

        return render(request, self.template_name, {"object_list": object_list, "overall": overall})


class ListItemType(ListView):
    model = ItemType
    template_name = "partials/labs/item/list_item_type.html"


class AddItemType(CreateView):
    model = ItemType
    form_class = AddItemTypeForm
    template_name = "partials/labs/item/add_item_type.html"
    success_url = reverse_lazy("labs:item-type-index")


class DeleteItemType(DeleteView):
    model = ItemType
    template_name = "partials/labs/delete_view.html"
    success_url = reverse_lazy("labs:item-type-index")
