from django.db import models
from client.models import Client


# Create your models here.

class Lab(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_due(self):
        total_paid = 0
        for payment in self.item_set.all():
            total_paid += payment.amount
        return total_paid


class ItemType(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Item(models.Model):
    lab = models.ForeignKey(Lab)
    client = models.ForeignKey(Client)
    itemtype = models.ForeignKey(ItemType)
    amount = models.FloatField(default=0, blank=False, null=False)
    count = models.IntegerField(default=1, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.itemtype.title
