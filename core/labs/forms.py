from django import forms
from .models import Lab, Item, ItemType


class AddLabForm(forms.ModelForm):
    class Meta:
        model = Lab
        fields = ['title']
        labels = {
            "title": "الاسم"
        }
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
        }


class AddItemToLabForm(forms.ModelForm):
    class Meta:
        model = Item
        labels = {
            "lab": "المعمل", "client": "العميل", "itemtype": "التركيبة", "count": "العدد", "amount": "السعر"
        }

        fields = ['lab', 'client', 'itemtype', 'count', 'amount', ]
        widgets = {
            'lab': forms.Select(attrs={'class': 'form-control'}),
            'client': forms.Select(attrs={'class': 'form-control'}),
            'itemtype': forms.Select(attrs={'class': 'form-control'}),
            'count': forms.NumberInput(attrs={'class': 'form-control'}),
            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
        }


class AddItemTypeForm(forms.ModelForm):
    class Meta:
        model = ItemType
        labels = {
            "title": "اسم التركيبة",
        }

        fields = ['title']
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
        }
