from .models import Appointment
from  django.db.models import Q
from django.shortcuts import render
from django.contrib import messages
from .forms import AddAppointmentForm
from django.urls import reverse, reverse_lazy
from django.http import JsonResponse, HttpResponseRedirect
from django.views.generic import View, CreateView, ListView, DeleteView


# Create your views here.
class ListAppointments(ListView):
    model = Appointment
    template_name = "partials/appointments/list_view.html"


class AddAppointment(View):
    template_name = "partials/appointments/add_view.html"
    form_class = AddAppointmentForm

    def get(self, request, *args, **kwargs):
        context = {'form': self.form_class}
        return render(request, self.template_name, context)

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        appointment = request.POST.get('appointment')
        date = request.POST.get('date')
        appointments = Appointment.objects.filter(
            Q(date=date) &
            Q(appointment=appointment)
        )

        # Q(date__icontains=date) |
        # Q(appointment__icontains=appointment)

        if appointments.count() == 0:
            if form.is_valid():
                appointment = form.save(commit=False)
                appointment.save()
                messages.success(
                    request, 'Appointment to user %s has been added.' % appointment.client.name)
            return HttpResponseRedirect(reverse('appointment:list'))

        messages.success(
            request, 'Sorry this (%s) appointment has been taken for today.' % appointment)
        return HttpResponseRedirect(reverse('appointment:create'))

    def get_success_url(self):
        return reverse('appointment:list')


class AppointmentDeleteView(DeleteView):
    model = Appointment
    template_name = "partials/appointments/delete_confirm.html"
    success_url = reverse_lazy("appointment:list")
