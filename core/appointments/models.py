from django.db import models
from client.models import Client


# Create your models here.

class Appointment(models.Model):
    client = models.ForeignKey(Client)
    appointment = models.TimeField(null=False, blank=False)
    date = models.DateField(null=False, blank=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.client.name+"'s" + " appointment"
