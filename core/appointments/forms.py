from django import forms
from .models import Appointment


class AddAppointmentForm(forms.ModelForm):
    class Meta:
        model = Appointment
        labels = {
            "client":"المريض","appointment":"المعاد","date":"التاريخ"
        }

        fields = ['client', 'appointment', 'date', ]
        widgets = {
            'client': forms.Select(attrs={'class': 'form-control'}),
            'appointment': forms.TextInput(attrs={'id': 'timePicker', 'class': 'form-control'}),
            'date': forms.TextInput(attrs={'id': 'timePicker', 'type': 'date', 'class': 'form-control'}),
        }
